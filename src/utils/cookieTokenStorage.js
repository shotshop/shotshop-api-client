/* eslint-disable no-undef */

const COOKIE_NAME = 'login';

const setCookie = (name, value, days) => {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
};

const getCookie = (name) => {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
  }

  return false;
};

export const clearLoginDataFromCookie = () => {
  setCookie(COOKIE_NAME, '', 1);
};

export const getLoginDataFromCookie = () => {
  const json = getCookie(COOKIE_NAME);
  if (!json || json.length === 0) {
    return false;
  }

  if (json && json.length > 0) {
    try {
      const loginData = JSON.parse(json);

      if (loginData) {
        return {
          isLoggedIn: true,
          ...loginData,
        };
      }
    } catch (e) {
      clearLoginDataFromCookie();
    }
  }

  return false;
};

export const getJwtToken = () => {
  const data = getLoginDataFromCookie();
  if (data) {
    return data.token;
  }

  return false;
};

export const setLoginDataCookie = ({
  name, email, scope, token
}) => {
  setCookie(COOKIE_NAME, JSON.stringify({
    name,
    email,
    scope,
    token,
  }), 1);
};

export default {
  clearLoginDataFromCookie,
  getLoginDataFromCookie,
  getJwtToken,
  setLoginDataCookie,
};
