import EventEmitter from 'events';

let eventEmitter = false;

export const clearEventEmitter = () => {
  eventEmitter = false;
};

export const getEventEmitter = () => {
  if (!eventEmitter) {
    eventEmitter = new EventEmitter();
  }

  return eventEmitter;
};

export const registerEventListener = (event, callback) => getEventEmitter().on(event, callback);

export const emitEvent = (event, payload) => getEventEmitter().emit(event, payload);

export default {
  getEventEmitter,
  registerEventListener,
  emitEvent,
};
