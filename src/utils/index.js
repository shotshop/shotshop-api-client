import events from './events';
import cookieTokenStorage from './cookieTokenStorage';

export default {
  events,
  cookieTokenStorage,
};
