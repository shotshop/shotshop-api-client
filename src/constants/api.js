export const ENDPOINT_MEMBERS = '/members';
export const ENDPOINT_MEMBERS_ME = `${ENDPOINT_MEMBERS}/me`;
export const ENDPOINT_AUTH_TOKENS = '/auth-tokens';
export const ENDPOINT_FORMATS = '/formats';
export const ENDPOINT_SETTINGS = '/settings';
export const ENDPOINT_ALBUMS = '/albums';
export const ENDPOINT_UPLOAD_TOKEN = '/upload-tokens';
export const ENDPOINT_PHOTOS = '/photos';
