import { ENDPOINT_ALBUMS } from '../constants/api';
import { get, post, put } from './request';

export const album = id => get(`${ENDPOINT_ALBUMS}/${id}`);

export const albumList = parentId => get(`${ENDPOINT_ALBUMS}${parentId ? `?parentId=${parentId}` : ''}`);

export const createAlbum = album => post(ENDPOINT_ALBUMS, album);

export const updateAlbum = album => put(ENDPOINT_ALBUMS, album.id, album);

export default {
  album,
  albumList,
  createAlbum,
  updateAlbum
};
