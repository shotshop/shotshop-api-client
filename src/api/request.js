import { emitEvent } from '../utils/events';
import { EVENT_API_FORBIDDEN, EVENT_API_ERROR, EVENT_API_VALIDATION_ERROR } from '../constants/events';
import { clearLoginDataFromCookie, getJwtToken } from '../utils/cookieTokenStorage';

let apiBaseUrl = false;
export const setApiBaseUrl = (url) => { apiBaseUrl = url; };
export const getApiBaseUrl = () => {
  if (!apiBaseUrl) {
    throw new Error('API used before base url set');
  }

  return apiBaseUrl;
};

export const handleSuccess = async (response) => {
  let json = { success: true, data: {} };
  if (response.status === 200) {
    json = await response.json();
  }

  return json.data;
};

export const handleError = async (response) => {
  // eslint-disable-next-line no-console
  console.error('API Error', response);
  switch (response.status) {
    case 400:
      emitEvent(EVENT_API_VALIDATION_ERROR, await response.json());
      return;
    case 403:
      clearLoginDataFromCookie();
      emitEvent(EVENT_API_FORBIDDEN);
      return;
    default:
      emitEvent(EVENT_API_ERROR, response);
  }
};

export const request = (
  type,
  endpoint,
  body,
  contentType = 'application/json; charset=UTF-8',
  headers = {},
) => {
  const jwtToken = getJwtToken();
  if (jwtToken) {
    headers.Authorization = `Bearer ${jwtToken}`;
  }

  if (contentType) {
    headers['Content-type'] = contentType;
  }

  // eslint-disable-next-line no-undef
  return fetch(`${getApiBaseUrl()}${endpoint}`, {
    method: type,
    headers,
    body,
  })
    .then((response) => {
      if (response.ok) {
        return handleSuccess(response);
      }

      return handleError(response);
    });
};

export const get = endpoint => request('get', endpoint, undefined);

export const post = (endpoint, data) => request('post', endpoint, JSON.stringify(data));

export const put = (endpoint, id, data) => request('put', `${endpoint}/${id}`, JSON.stringify(data));

export default {
  setApiBaseUrl,
  getApiBaseUrl,
  handleSuccess,
  handleError,
  request,
  get,
  post,
  put,
};
