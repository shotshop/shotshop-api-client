import { ENDPOINT_FORMATS } from '../constants/api';
import { get, post, put } from './request';

export const formatList = () => get(ENDPOINT_FORMATS);

export const updateFormat = format => put(ENDPOINT_FORMATS, format.id, format);

export const createFormat = format => post(ENDPOINT_FORMATS, format);

export default {
  formatList,
  updateFormat,
  createFormat
};
