import { get, post } from './request';
import { ENDPOINT_AUTH_TOKENS, ENDPOINT_MEMBERS_ME } from '../constants/api';
import { setLoginDataCookie } from '../utils/cookieTokenStorage';

const handleMemberResponse = (member) => {
  setLoginDataCookie(member);
  return member;
};

export const login = (email, password) => post(ENDPOINT_AUTH_TOKENS, { email, password })
  .then(handleMemberResponse);

export const me = () => get(ENDPOINT_MEMBERS_ME).then(handleMemberResponse);

export default {
  login,
  me
};
