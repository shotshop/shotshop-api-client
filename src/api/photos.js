import { get, post, put } from './request';
import { ENDPOINT_PHOTOS, ENDPOINT_UPLOAD_TOKEN } from '../constants/api';

export const photoList = albumId => get(`${ENDPOINT_PHOTOS}${albumId ? `?albumId=${albumId}` : ''}`);

export const updatePhoto = photo => put(ENDPOINT_PHOTOS, photo.id, photo);

export const createUploadToken = (albumId, filename) => post(`${ENDPOINT_UPLOAD_TOKEN}`, { albumId, filename });

export default {
  photoList,
  updatePhoto,
  createUploadToken
};
