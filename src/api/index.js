import albumEndpoints from './albums';
import formatEndpoints from './formats';
import memberEndpoints from './member';
import photoEndpoints from './photos';
import settingEndpoints from './settings';

export default {
  ...albumEndpoints,
  ...formatEndpoints,
  ...memberEndpoints,
  ...photoEndpoints,
  ...settingEndpoints,
};
