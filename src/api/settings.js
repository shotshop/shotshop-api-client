import { ENDPOINT_SETTINGS } from '../constants/api';
import { get, post, put } from './request';

export const settingList = () => get(ENDPOINT_SETTINGS);

export const createSetting = setting => post(ENDPOINT_SETTINGS, setting);

export const updateSetting = setting => put(ENDPOINT_SETTINGS, setting.name, setting);

export default {
  settingList,
  createSetting,
  updateSetting
};
