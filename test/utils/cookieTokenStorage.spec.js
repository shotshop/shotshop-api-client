/* eslint-disable no-undef */

import { JSDOM } from 'jsdom';
import { clearLoginDataFromCookie, getLoginDataFromCookie, setLoginDataCookie } from '../../src/utils/cookieTokenStorage';

describe('cookieTokenStorage', () => {
  beforeEach(() => {
    const dom = new JSDOM();
    global.document = dom.window.document;
    global.window = dom.window;

    clearLoginDataFromCookie();
  });

  describe('clearLoginDataFromCookie', () => {
    it('should clear login cookie', () => {
      document.cookie = 'login=testtestest';
      clearLoginDataFromCookie();

      expect(document.cookie).toBe('login=');
    });

    it('should not modify other cookies', () => {
      document.cookie = 'test=blah;login=testtestest';
      clearLoginDataFromCookie();

      expect(document.cookie).toContain('test=blah');
    });
  });

  describe('setLoginDataCookie', () => {
    it('should set login data correctly on the cookie', () => {
      setLoginDataCookie({
        name: 'test', email: 'test@test.com', scope: 'admin', token: 'abc123'
      });

      expect(document.cookie).toContain('login={"name":"test","email":"test@test.com","scope":"admin","token":"abc123"}');
    });
  });

  describe('getLoginDataFromCookie', () => {
    it('should set login data correctly on the cookie', () => {
      document.cookie = 'login={"name":"test","email":"test@test.com","scope":"admin","token":"abc123"}';
      const {
        name, email, scope, token
      } = getLoginDataFromCookie();

      expect(name).toBe('test');
      expect(email).toBe('test@test.com');
      expect(scope).toBe('admin');
      expect(token).toBe('abc123');
    });

    it('should return false when cookie is empty', () => {
      const result = getLoginDataFromCookie();
      expect(result).toBe(false);
    });
  });
});
