import { clearEventEmitter, emitEvent, getEventEmitter, registerEventListener } from '../../src/utils/events';

describe('events', () => {
  beforeEach(() => {
    clearEventEmitter();
  });

  describe('getEventEmitter', () => {
    it('should always return the same instance of event emitter', () => {
      getEventEmitter().tmp = 'test';

      const emitter = getEventEmitter();
      expect(emitter.tmp).toBe('test');
    });
  });

  describe('registerEventListener', () => {
    it('should register event on the event emitter', () => {
      expect(getEventEmitter().eventNames()).not.toContain('test-event');
      registerEventListener('test-event', () => {});
      expect(getEventEmitter().eventNames()).toContain('test-event');
    });
  });

  describe('emitEvent', () => {
    const callback = jest.fn();
    getEventEmitter().on('test-event', callback);

    expect(callback).not.toHaveBeenCalled();

    emitEvent('test-event');
    expect(callback).toHaveBeenCalled();

    emitEvent('test-event', 'blah');
    expect(callback).toHaveBeenCalledWith('blah');
  });
});
