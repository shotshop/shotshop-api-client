/* eslint-disable no-console */
import { getApiBaseUrl, handleError, setApiBaseUrl } from '../../src/api/request';
import { EVENT_API_ERROR, EVENT_API_FORBIDDEN, EVENT_API_VALIDATION_ERROR } from '../../src/constants/events';
import { getEventEmitter } from '../../src/utils/events';

const mockResponse = (status, data = {}) => ({
  status,
  json: async () => data
});

const ensureEmitsEvent = async (event, callback, eventData = undefined) => {
  const emitter = getEventEmitter();
  let eventReceived = false;
  let eventDataReceived = false;
  emitter.on(event, (data) => {
    eventReceived = true;
    eventDataReceived = data;
  });

  await callback();

  expect(eventReceived).toBeTruthy();
  if (eventData !== undefined) {
    expect(eventDataReceived).toBe(eventData);
  }
};

describe('handleError', async () => {
  beforeEach(() => {
    console.error = () => {};
    getEventEmitter().removeAllListeners();
  });

  describe('when receiving any error', async () => {
    it('should always log the error with response', async () => {
      let errorMessage = false;
      let errorResponse = false;
      console.error = (message, response) => {
        errorMessage = message;
        errorResponse = response;
      };

      const response = mockResponse(500);
      await handleError(response);

      expect(errorMessage).toBe('API Error');
      expect(errorResponse).toBe(response);
    });
  });

  describe('when receiving a 403 error', async () => {
    it('should emit correct event', async () => {
      await ensureEmitsEvent(
        EVENT_API_FORBIDDEN,
        async () => handleError(mockResponse(403))
      );
    });
  });

  describe('when receiving a 500 error', async () => {
    it('should emit correct event', async () => {
      await ensureEmitsEvent(
        EVENT_API_ERROR,
        async () => handleError(mockResponse(500))
      );
    });
  });

  describe('when receiving a 400 error', async () => {
    it('should emit correct event', async () => {
      await ensureEmitsEvent(
        EVENT_API_VALIDATION_ERROR,
        async () => handleError(mockResponse(400))
      );
    });

    it('should attach response data to emitted event', async () => {
      await ensureEmitsEvent(
        EVENT_API_VALIDATION_ERROR,
        async () => handleError(mockResponse(400, 'test')),
        'test'
      );
    });
  });
});

describe('setApiBaseUrl', () => {
  it('should set api url', () => {
    setApiBaseUrl('abc123');
    expect(getApiBaseUrl()).toBe('abc123');
  });
});
